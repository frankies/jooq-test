package com.jooq;

import org.jooq.DSLContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jooq.generate.tables.daos.AuthorDao;
import com.jooq.generate.tables.daos.RoleDao;
import com.jooq.generate.tables.daos.UserDao;
import com.jooq.generate.tables.daos.UserRoleCkDao;
import com.jooq.generate.tables.daos.UserRoleDao;

/**
 *
 * @author YMSLX
 * @version 1.0
 *
 */
@Configuration
public class DaoConfiguration {
    
    @Bean
    public UserDao userDao(DSLContext dsl) {
        return new UserDao(dsl.configuration());
    }
    
    @Bean
    public RoleDao roleDao(DSLContext dsl) {
        return new RoleDao(dsl.configuration());
    }
    
    @Bean
    public UserRoleDao userRoleDao(DSLContext dsl) {
        return new UserRoleDao(dsl.configuration());
    }
    
    @Bean
    public UserRoleCkDao userRoleCkDao(DSLContext dsl) {
        return new UserRoleCkDao(dsl.configuration());
    }
    
    @Bean
    public AuthorDao authorDao(DSLContext dsl) {
        return new AuthorDao(dsl.configuration());
    }
    
}
