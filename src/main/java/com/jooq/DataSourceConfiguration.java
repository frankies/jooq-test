package com.jooq;

import javax.sql.DataSource;

import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.ExecuteListener;
import org.jooq.SQLDialect;
import org.jooq.conf.Settings;
import org.jooq.conf.StatementType;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.jooq.listener.DeleteOrUpdateWithoutWhereListener;
import com.jooq.listener.PrettyPrinter;
import com.jooq.listener.StatisticsListener;

/**
 *
 * @author amoy
 * @version 1.0
 *
 */
@org.springframework.context.annotation.Configuration
@EnableTransactionManagement
@PropertySource("classpath:jdbc.properties")
public class DataSourceConfiguration {

    @Value("${datasource.jdbc.url}")
    private String url;

    @Value("${datasource.jdbc.username}")
    private String username;

    @Value("${datasource.jdbc.password}")
    private String password;
    
    @Bean
    public DataSource dataSource() {
        return new DriverManagerDataSource(url, username, password);
    }
    
//    @Bean
//    public DataSource dataSource() {
//        HikariConfig config = new HikariConfig();
//        config.setJdbcUrl("jdbc:p6spy:h2:~/demo-jooq");
//        config.setUsername("sa");
//        config.setPassword("sa");
//        return new TransactionAwareDataSourceProxy(new HikariDataSource(config));
//    }

    //  @Bean
//  public TransactionManager txtMgn() {
//      return new JdbcTransactionManager(dataSource());
//  }
    

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public JdbcTemplate jdbcTmpl() {
        return new JdbcTemplate(dataSource());
    }
}
