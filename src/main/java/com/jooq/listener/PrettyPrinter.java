package com.jooq.listener;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.jooq.DSLContext;
import org.jooq.ExecuteContext;
import org.jooq.ExecuteType;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultExecuteListener;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author amoy
 * @version 1.0
 *
 */
@Slf4j
public class PrettyPrinter extends DefaultExecuteListener {

    /**
     * Generated UID
     */
    private static final long serialVersionUID = 7399239846062763212L;

    public static final Map<ExecuteType, Integer> STATISTICS = new ConcurrentHashMap<>();

    @Override
    public void executeStart(ExecuteContext ctx) {
        // Create a new DSLContext for logging rendering purposes
        // This DSLContext doesn't need a connection, only the SQLDialect...
        DSLContext create = DSL.using(ctx.dialect(),

                // ... and the flag for pretty-printing
                new Settings().withRenderFormatted(true));

        // If we're executing a query
        if (ctx.query() != null) {
           log.info(create.renderInlined(ctx.query()));
        }

        // If we're executing a routine
        else if (ctx.routine() != null) {
            log.info(create.renderInlined(ctx.routine()));
        }
    }
}
