package com.jooq.listener;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.jooq.ExecuteContext;
import org.jooq.ExecuteType;
import org.jooq.impl.DefaultExecuteListener;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author amoy
 * @version 1.0
 *
 */
@Slf4j
public class StatisticsListener extends DefaultExecuteListener {

    /**
     * Generated UID
     */
    private static final long serialVersionUID = 7399239846062763212L;

    public static final Map<ExecuteType, Integer> STATISTICS = new ConcurrentHashMap<>();

    @Override
    public void start(ExecuteContext ctx) {
        STATISTICS.compute(ctx.type(), (k, v) -> v == null ? 1 : v + 1);
    }
    
    @Override
    public void end(ExecuteContext ctx) {
        log.info("STATISTICS");
        log.info("----------");

        for (ExecuteType type : ExecuteType.values()) {
            log.info("{} {}", type.name(), StatisticsListener.STATISTICS.get(type) + " executions");
        }
    }
    
}
