package com.jooq.listener;

import org.jooq.ExecuteContext;
import org.jooq.impl.DefaultExecuteListener;

/**
 *
 * @author amoy
 * @version 1.0
 *
 */
public class DeleteOrUpdateWithoutWhereListener extends DefaultExecuteListener {

    @Override
    public void renderEnd(ExecuteContext ctx) {
        if (ctx.sql().matches("^(?i:(UPDATE|DELETE)(?!.* WHERE ).*)$")) {
            throw new DeleteOrUpdateWithoutWhereException();
        }
    }
    
    public static class DeleteOrUpdateWithoutWhereException extends RuntimeException {}
}
