package com.jooq;

import javax.sql.DataSource;

import org.jooq.DSLContext;
import org.jooq.ExecuteListener;
import org.jooq.SQLDialect;
import org.jooq.conf.Settings;
import org.jooq.conf.StatementType;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.jooq.listener.DeleteOrUpdateWithoutWhereListener;
import com.jooq.listener.PrettyPrinter;
import com.jooq.listener.StatisticsListener;

/**
 *
 * @author YMSLX
 * @version 1.0
 *
 */
@Configuration
@Import({DataSourceConfiguration.class, DaoConfiguration.class})
public class JooqConfiguration {
    
    @Bean
    public DSLContext dslContext(org.jooq.Configuration configuration) {
        return DSL.using(configuration);
    }

    @Bean
    public org.jooq.Configuration configuration(DataSource dataSource) {
        org.jooq.Configuration configuration = new DefaultConfiguration();
        configuration.set(SQLDialect.H2);
        configuration.set(dataSource);
        configWithSettings(configuration);
        configWithExecuteListeners(configuration);
        return configuration;
    }
    
    /**
     * @param configuration
     */
    private void configWithExecuteListeners(org.jooq.Configuration configuration) {
        //统计执行情况
        ExecuteListener statisticsListener = new StatisticsListener();
        
        //打印SQL
        ExecuteListener prettyPrinter = new PrettyPrinter();
        
        //禁止update/where 不带条件
        ExecuteListener deleteOrUpdateNoWhere = new DeleteOrUpdateWithoutWhereListener();
        
        configuration.set(/*statisticsListener, prettyPrinter,*/ deleteOrUpdateNoWhere);
    }

    /**
     * @param configuration
     */
    private void configWithSettings(org.jooq.Configuration configuration) {
        Settings settings = configuration.settings();
        settings.setStatementType(StatementType.STATIC_STATEMENT);
        settings.withBatchSize(100); // Default Integer.MAX_VALUE
        settings.setRenderSchema(false); //这里SQL语句就不带schema
        configuration.set(settings);
    }
}
