
drop table if exists `author` ;
CREATE TABLE `author` (
  `id` int NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- 多对多场景里的“1”
drop table if exists `USER` ;
CREATE TABLE USER (userId int auto_increment PRIMARY key, ID INT, NAME VARCHAR(50), UPDATE_COUNTER int default 0);
INSERT INTO user(id, name) values(1, 'l'), 
(2, 'Johnny Warlker'),
(3, 'Johnny Warlker2'),
(5, 'Johnny Warlker3'),
(6, 'Johnny Warlker'),
(4, 'oo');
 
-- 多对多场景里的“1”
drop table if exists `ROLE` ;
CREATE TABLE ROLE (ID int auto_increment PRIMARY key,  role_name varchar(40));
INSERT INTO ROLE(role_name) values('admin'), ('anony'), ('sysuser'), ('officer'); 
 
-- 多对多场景里的“多”
-- 主键是单个字段ID
drop table if exists `USER_ROLE` ;
CREATE TABLE USER_ROLE (ID int auto_increment PRIMARY key,  ROLE_ID INT, USER_ID INT);
INSERT INTO USER_ROLE(USER_ID, ROLE_ID) VALUES(1, 2), (1,1), (2,3), (3,1);
 

-- 主键是组合字段 
drop table if exists `USER_ROLE_CK` ;
CREATE TABLE USER_ROLE_CK (
		 ROLE_ID INT, 
		 USER_ID INT, 
		 DESCRIPT VARCHAR(40),
		  PRIMARY KEY (ROLE_ID, USER_ID) 
 ); 
INSERT INTO USER_ROLE_CK(USER_ID, ROLE_ID, DESCRIPT) VALUES(1, 2, 'd-1' ), (1,1, 'd-1' ), (2,3, 'd-1' ), (3,1, 'd-1' );
 