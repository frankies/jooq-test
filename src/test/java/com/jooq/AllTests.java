package com.jooq;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 *
 * @author amoy
 * @version 1.0
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ JooqCurdTest.class, 
    JooqFetchTest.class,
    JooqFromTest.class,
    JooqToTest.class,
    JooqDaoTest.class,
    JooqExtendPageTest.class})
public class AllTests {

}
