package com.jooq;

import static com.jooq.generate.tables.TUser.USER;

import org.assertj.core.api.JUnitSoftAssertions;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import com.jooq.generate.tables.records.UserRecord;

/**
*
* @author amoy
* @version 1.0
*
*/
@RunWith(SpringRunner.class)
@ContextConfiguration(classes =JooqConfiguration.class )
@Sql("/db.sql")
public class BaseTest {

    @Autowired
    protected DSLContext dsl;
    
    @Rule
    public JUnitSoftAssertions softAssertions = new JUnitSoftAssertions();
    
    /**
     * @return
     */
    protected Result<UserRecord> finaAll() {
        Result<UserRecord> us = dsl.fetch(USER);
        return us;
    }

    /**
     * @return
     */
    protected int countTable() {
        return dsl.fetchCount(USER);
    }
}
