package com.jooq;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.jooq.generate.tables.daos.UserDao;
import com.jooq.generate.tables.pojos.UserPojo;
import com.jooq.generate.tables.pojos.UserPojoAssert;

/**
 * 
 *   如果没有主键的表，在代码生成时，是不会生成DAO的
 * 
 * @author amoy
 * @version 1.0
 *
 */
public class JooqDaoTest extends BaseTest {


    @Autowired
    private UserDao userDao;

    @Test
    public void testFind() {
        UserPojo u = userDao.fetchOneByUserid(1);
        softAssertions.assertThat(u).isNotNull();
        
        //利用生成
        softAssertions.assertThat(u).extracting(UserPojo::getName)
                                                 .isEqualTo("l");
        
        UserPojoAssert.assertThat(u).hasUserid(1);
        
        u = userDao.findById(1);
        softAssertions.assertThat(u).isNotNull();
        

        List<UserPojo> us = userDao.fetchByName("l");
        softAssertions.assertThat(us).isNotEmpty(); 
        
        softAssertions.assertThat(us).extracting(UserPojo::getName)
                            .contains("l")
                            .doesNotContain("xxxs");
        
        //
        softAssertions.assertThat(userDao.findAll().size()).isEqualTo(this.countTable());
    }

}
