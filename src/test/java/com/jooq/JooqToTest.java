package com.jooq;

import static com.jooq.generate.tables.TUser.USER;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.jooq.Record2;
import org.junit.Test;

import com.jooq.generate.tables.pojos.UserPojo;

/**
 *
 * @author amoy
 * @version 1.0
 *
 */
public class JooqToTest extends BaseTest {

    /**
     * 查出的结果组装成POJO
     */
    @Test
    public void testToPojo() {
        UserPojo u = dsl.fetchAny(USER).into(UserPojo.class);
        assertNotNull(u);
        assertNotNull(u.getUserid());
        assertTrue(u.getUserid() > 0);
    }
    
    /**
     * 根据自定字段装到Record里。
     */
    @Test
    public void testToRecord() {
        Record2<Integer, String> row = dsl.fetchAny(USER).into(USER.ID, USER.NAME);
        assertNotNull(row);
        assertNotNull(row.get(0));
        assertNotNull(row.get(1));
    }
    
    /**
     * 根据自定字段装到Object[]
     */
    @Test
    public void testToArray() {
        Object[]  row = dsl.fetchAny(USER).intoArray();
        assertNotNull(row);
        assertNotNull(row[0]);
        assertNotNull(row[1]);
    }
    
    /**
     * 根据自定字段装到Map
     */
    @Test
    public void testToMap() {
        Map<String, Object>  row = dsl.fetchAny(USER).intoMap();
        assertNotNull(row);
        assertNotNull(row.get(USER.NAME.getName()));
        assertNotNull(row.get(USER.ID.getName()));
    }
    
    /**
     * 根据自定字段装到ResultSet
     * @throws SQLException 
     */
    @Test
    public void testToResultSet() throws SQLException {
        ResultSet rs = dsl.fetch(USER).intoResultSet();
        while (rs.next()) {
            assertNotNull(rs);
            assertNotNull(rs.getString(USER.NAME.getName()));
            assertNotNull(rs.getInt(USER.ID.getName()));
        }
    }

}
