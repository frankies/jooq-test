package com.jooq;

import static com.jooq.generate.tables.TUser.USER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;

import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Result;
import org.jooq.exception.DataAccessException;
import org.junit.Test;

import com.jooq.generate.tables.records.UserRecord;
import com.jooq.listener.DeleteOrUpdateWithoutWhereListener.DeleteOrUpdateWithoutWhereException;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author amoy
 * @version 1.0
 *
 */
@Slf4j
public class JooqCurdTest extends BaseTest {

    @Test
    public void testJooqSelectAll() throws SQLException {
        Result<UserRecord> u = dsl.fetch(USER);
        assertNotNull(u);
        log.info("====>size: {}", u.size());
        assertTrue(u.size() > 0);
    }
    
    @Test
    public void testJooqGet() throws SQLException {
        Record u = dsl.selectFrom(USER)
                                       .limit(1)
                                       .fetchOne();
        assertNotNull(u);
        String n = u.get(USER.NAME);
        assertNotNull(n);
        
        u = dsl.selectFrom(USER)
                    .fetchAny(); 
        assertNotNull(u);
        
        Long i = u.get(USER.USERID, Long.class);
        assertNotNull(i);
    }
    
    @Test
    public void testJooqGetRecordN() throws SQLException {
        Record2<String, Integer> record2 = dsl.select(USER.NAME, USER.ID)
                                                                              .from(USER)
                                                                                 .fetchAny();
        String name = record2.value1();
        Integer id = record2.value2(); 
        assertNotNull(name);
        assertNotNull(id);
    }
    
    

    @Test
    public void testJooqSelectOne() throws SQLException {
        UserRecord u = dsl.selectFrom(USER).where(USER.ID.eq(1)).fetchOne();
        assertNotNull(u);
    }
    
    @Test
    public void testJooqSelectOneChanged() throws SQLException {
        UserRecord u = dsl.selectFrom(USER).where(USER.ID.eq(1)).fetchOne();
        String on = u.getName();
        u.setName(null);
        u.changed(USER.NAME, false); //这里标记 username 字段不更新
        u.update(); //更新记录
        
        
        u.setName("dkdkkdkdk" );
        u.reset(USER.NAME);//这里标记 username 字段不更新
        u.update(); //更新记录
        
        //
      String n =  dsl.selectFrom(USER)
            .where(USER.ID.eq(1))
            .fetchOne(USER.NAME);
      assertTrue(n.equals(on));
      
      Record1<String>   nr =  dsl.select(USER.NAME).from(USER)
                              .where(USER.ID.eq(1))
                              .fetchOne();
    
        assertTrue(nr.getValue(0).equals(on));
    }

    @Test
    public void testJooqSelect() throws SQLException {
        Result<UserRecord> result = dsl.selectFrom(USER).where(USER.NAME.like("Johnny%")).orderBy(USER.ID).fetch();
        log.info("{} {}", result.size(), new Date());
        result.stream().forEach(r -> log.info("==> id:{}, name:{}", r.getId(), r.getName()));

    }

    /**
     * 
     *  插入后可以取得自增主键的值
     *  
     * @throws DataAccessException
     * @throws SQLException
     */
    @Test
    public void testJooqInsertsReturn() throws DataAccessException, SQLException {

        UserRecord record = dsl.newRecord(USER);
        record.setId(1003);
        record.setName("test-1991");
        record.insert();

        int before = record.getUserid();
        assertTrue(record.getUserid() >= 0);


        // 这里可以取得Insert之后的主键值
        int counter = dsl.insertInto(USER, USER.ID, USER.NAME).values(484, "name").returning(USER.USERID).fetchOne().getUserid();

        assertEquals(counter, before + 1);
    }

    @Test
    public void testJooqInsertsDuplicateIngore() throws DataAccessException, SQLException {

        int before = countTable();
        dsl.insertInto(USER, USER.USERID, USER.NAME).values(1, "name").onDuplicateKeyIgnore().execute();
        int after = countTable();
        log.info("Before {}, after {}", after);
        assertEquals(before, after);
    }

    @Test
    public void testJooqInsertsDuplicateUpdate() throws DataAccessException, SQLException {

        int before = countTable();
        String actual = "Have-change-me";
        dsl.insertInto(USER, USER.USERID, USER.NAME).values(1, "name").onDuplicateKeyUpdate().set(USER.NAME, actual).execute();
        int after = countTable();
        log.info("Before {}, after {}", after);
        assertEquals(before, after);

        //
        String name = dsl.selectFrom(USER).where(USER.USERID.eq(1)).fetchOne().getName();

        assertEquals(name, actual);

    }
    
    @Test
    public void testJooqBatchUpdate2() throws DataAccessException, SQLException {

        dsl.batched(c -> {
            for (int i = 0; i < 100; i++) {
                dsl.update(USER).set(USER.UPDATE_COUNTER, USER.UPDATE_COUNTER.plus(1)).where(USER.NAME.likeRegex("^l.*")).execute();
            }
        });
    }
    
    @Test
    public void testJooqBatchInsert() {
        
       int oc = this.countTable();
     // 1. several queries
     // ------------------
     dsl.batch(
             dsl.insertInto(USER, USER.ID, USER.NAME).values(100, "dsl-100"),
             dsl.insertInto(USER, USER.ID, USER.NAME).values(101, "dsl-101"),
             dsl.insertInto(USER, USER.ID, USER.NAME).values(102, "dsl-102")
             )
     .execute();
     assertThat(this.countTable()).isEqualTo(oc + 3);
     
     String n = dsl.selectFrom(USER).where(USER.ID.eq(100)).fetchOne(USER.NAME);
     assertThat(n).isEqualTo("dsl-100");
     
     // 2. a single query
     // -----------------
     dsl.batch(dsl.insertInto(USER, USER.ID, USER.NAME).values((Integer) null, null))
                         .bind(200 , "dsl-200"    )
                         .bind(201 , "dsl-201"    )
                           .bind(202 , "dsl-202"    )
                               .execute();
     assertThat(this.countTable()).isEqualTo(oc + 6);
     
     n = dsl.selectFrom(USER).where(USER.ID.eq(200)).fetchOne(USER.NAME);
     assertThat(n).isEqualTo("dsl-200");
    }

    @Test
    public void testJooqInserts() throws DataAccessException, SQLException {

        Result<UserRecord> us = finaAll();
        int i = Long.valueOf(System.currentTimeMillis() % 100).intValue();
        dsl.insertInto(USER, USER.ID, USER.NAME).values(Long.valueOf(i).intValue(), "Johnny-" + i).values(Long.valueOf(i + 1).intValue(), "sa-" + i).values(Long.valueOf(i + 2).intValue(), "Joy-" + i).execute();
        log.info("Insert 3 rows!");
        Result<UserRecord> us2 = finaAll();
        assertTrue(us2.size() - us.size() == 3);
        log.info("before {}, now {}, {}", us.size(), us2.size(), us);

    }

    @Test
    public void testJooqInserts2() throws DataAccessException, SQLException {

        Result<UserRecord> us = finaAll();
        int i = Long.valueOf(System.currentTimeMillis() % 100).intValue();
        dsl.insertInto(USER).set(USER.ID, Long.valueOf(i).intValue()).set(USER.NAME, "Johnny-" + i).newRecord().set(USER.ID, Long.valueOf(i + 1).intValue()).set(USER.NAME, "sa-" + i).newRecord().set(USER.ID, Long.valueOf(i + 2).intValue())
                .set(USER.NAME, "Joy-" + i).execute();
        log.info("Insert 3 rows!");
        Result<UserRecord> us2 = finaAll();
        assertTrue(us2.size() - us.size() == 3);
        log.info("before {}, now {}, {}", us.size(), us2.size(), us);

    }
    

    @Test
    public void testJooqInsertsRecord() {
       UserRecord ur =   dsl.newRecord(USER);
        String val = "test-hello";
        ur.setName(val);
         ur.insert(USER.NAME);
         int id = ur.getUserid();
         //插入再查出来
        String name = dsl.selectFrom(USER)
             .where(USER.USERID.eq(id))
             .fetchOne(USER.NAME);
        assertTrue(name.equals(val));
    }

    @Test
    public void testJooqUpdates() throws DataAccessException, SQLException {
        Result<UserRecord> u = dsl.fetch(USER);
        u.stream().forEach(r -> {
            int j = Long.valueOf(System.currentTimeMillis() % 100).intValue();
            r.setUpdateCounter(r.getUpdateCounter() + 1);
            r.setName("change" + j);
            r.update();
        });
    }


    @Test(expected = DeleteOrUpdateWithoutWhereException.class)
    public void testJooqUpdateAbnormal() throws DataAccessException, SQLException {

        dsl.update(USER).set(USER.ID, 2).execute();

        log.info("Insert one row!");
    }

    @Test
    public void testJooqUpdateLikeSQL() throws DataAccessException, SQLException {

        dsl.update(USER).set(USER.ID, 30).set(USER.NAME, "test-39").where(USER.USERID.eq(Integer.valueOf(1))).execute();

        /////
        String fn = dsl.selectFrom(USER).where(USER.USERID.eq(Integer.valueOf(1))).fetchOne(USER.NAME);

        assertEquals("test-39", fn);
    }

    @Test
    public void testJooqUpdateInRecord() {

        UserRecord ur = dsl.newRecord(USER);
        ur.setUserid(1);
        ur.setName("test-39");
        ur.update();
        /////
        String fn = dsl.selectFrom(USER).where(USER.USERID.eq(Integer.valueOf(1))).fetchOne(USER.NAME);

        assertEquals("test-39", fn);
    }
    

    @Test
    public void testJooqFetchAndUpdate() {
        UserRecord ur = dsl.selectFrom(USER).where(USER.ID.eq(1)).fetchOne();
        ur.setName("Test haha");
        ur.update(); // 执行Update语句

        // 重新查询
        String n = dsl.selectFrom(USER).where(USER.ID.eq(1)).fetchOne(USER.NAME);
        assertTrue("Test haha".equals(n));
    }
    
    @Test
    public void testJooqFetchAndUpdateFields() {
        UserRecord ur = dsl.selectFrom(USER).where(USER.ID.eq(1)).fetchOne();
        int oid = ur.getId();
        ur.setName("Test haha");
        ur.setId(1000);
        ur.update(USER.NAME); // 执行Update语句，只更新Name字段

        // 重新查询
        int id = dsl.selectFrom(USER).where(USER.ID.eq(1)).fetchOne(USER.ID);
        assertTrue(id == oid);
    }

    @Test
    public void testJooqBatchUpdateInRecord() {

        UserRecord ur = new UserRecord();
        ur.setUserid(1);
        ur.setName("test-39");

        UserRecord ur2 = new UserRecord();
        ur2.setUserid(2);
        ur2.setName("test-1300");

        int[] cs = dsl.batchUpdate(Arrays.asList(ur, ur2)).execute();
        log.info("effects: {}", cs);
        /////
        String fn = dsl.selectFrom(USER).where(USER.USERID.eq(Integer.valueOf(2))).fetchOne(USER.NAME);

        assertEquals("test-1300", fn);
    }

    @Test
    public void deleteSQLLike() {

        int c1 = this.countTable();
        int c = dsl.delete(USER).where(USER.USERID.eq(Integer.valueOf(1))).execute();

        assertEquals(1, c);
        assertEquals(c1 - 1, this.countTable());

    }

    @Test
    public void deleteInRecordPK() {

        int c1 = this.countTable();

        UserRecord ur = dsl.newRecord(USER);
        ur.setUserid(1);
        int c = ur.delete();
        assertEquals(1, c);
        assertEquals(c1 - 1, this.countTable());

    }
    
    @Test
    public void deleteInRecordNoPK() {

        UserRecord ur = dsl.newRecord(USER); 
        int c = ur.delete(); //SQL:  delete from "PUBLIC"."USER" where "PUBLIC"."USER"."USERID" is null
        assertEquals(0, c); 
    }

    @Test
    public void batchDeleteInRecord() {

        int c1 = this.countTable();

        UserRecord ur1 = new UserRecord();
        ur1.setUserid(1);

        UserRecord ur2 = new UserRecord();
        ur2.setUserid(2);

        int[] c = dsl.batchDelete(Arrays.asList(ur1, ur2)).execute();
        assertEquals(2, c[0] + c[1]);
        assertEquals(c1 - 2, this.countTable());

    }

}
