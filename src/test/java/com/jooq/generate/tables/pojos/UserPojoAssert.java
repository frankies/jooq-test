package com.jooq.generate.tables.pojos;

/**
 * {@link UserPojo} specific assertions - Generated by CustomAssertionGenerator.
 *
 * Although this class is not final to allow Soft assertions proxy, if you wish to extend it, 
 * extend {@link AbstractUserPojoAssert} instead.
 */
@javax.annotation.Generated(value="assertj-assertions-generator")
public class UserPojoAssert extends AbstractUserPojoAssert<UserPojoAssert, UserPojo> {

  /**
   * Creates a new <code>{@link UserPojoAssert}</code> to make assertions on actual UserPojo.
   * @param actual the UserPojo we want to make assertions on.
   */
  public UserPojoAssert(UserPojo actual) {
    super(actual, UserPojoAssert.class);
  }

  /**
   * An entry point for UserPojoAssert to follow AssertJ standard <code>assertThat()</code> statements.<br>
   * With a static import, one can write directly: <code>assertThat(myUserPojo)</code> and get specific assertion with code completion.
   * @param actual the UserPojo we want to make assertions on.
   * @return a new <code>{@link UserPojoAssert}</code>
   */
  @org.assertj.core.util.CheckReturnValue
  public static UserPojoAssert assertThat(UserPojo actual) {
    return new UserPojoAssert(actual);
  }
}
