package com.jooq.generate.tables.pojos;

/**
 * Like {@link SoftAssertions} but as a junit rule that takes care of calling
 * {@link SoftAssertions#assertAll() assertAll()} at the end of each test.
 * <p>
 * Example:
 * <pre><code class='java'> public class SoftlyTest {
 *
 *     &#064;Rule
 *     public final JUnitBDDSoftAssertions softly = new JUnitBDDSoftAssertions();
 *
 *     &#064;Test
 *     public void soft_bdd_assertions() throws Exception {
 *       softly.assertThat(1).isEqualTo(2);
 *       softly.assertThat(Lists.newArrayList(1, 2)).containsOnly(1, 2);
 *       // no need to call assertAll(), this is done automatically.
 *     }
 *  }</code></pre>
 */
@javax.annotation.Generated(value="assertj-assertions-generator")
public class JUnitSoftAssertions extends org.assertj.core.api.JUnitSoftAssertions {

  /**
   * Creates a new "soft" instance of <code>{@link com.jooq.generate.tables.pojos.AuthorPojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.jooq.generate.tables.pojos.AuthorPojoAssert assertThat(com.jooq.generate.tables.pojos.AuthorPojo actual) {
    return proxy(com.jooq.generate.tables.pojos.AuthorPojoAssert.class, com.jooq.generate.tables.pojos.AuthorPojo.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link com.jooq.generate.tables.pojos.RolePojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.jooq.generate.tables.pojos.RolePojoAssert assertThat(com.jooq.generate.tables.pojos.RolePojo actual) {
    return proxy(com.jooq.generate.tables.pojos.RolePojoAssert.class, com.jooq.generate.tables.pojos.RolePojo.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link com.jooq.generate.tables.pojos.UserPojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.jooq.generate.tables.pojos.UserPojoAssert assertThat(com.jooq.generate.tables.pojos.UserPojo actual) {
    return proxy(com.jooq.generate.tables.pojos.UserPojoAssert.class, com.jooq.generate.tables.pojos.UserPojo.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link com.jooq.generate.tables.pojos.UserRoleCkPojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.jooq.generate.tables.pojos.UserRoleCkPojoAssert assertThat(com.jooq.generate.tables.pojos.UserRoleCkPojo actual) {
    return proxy(com.jooq.generate.tables.pojos.UserRoleCkPojoAssert.class, com.jooq.generate.tables.pojos.UserRoleCkPojo.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link com.jooq.generate.tables.pojos.UserRolePojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.jooq.generate.tables.pojos.UserRolePojoAssert assertThat(com.jooq.generate.tables.pojos.UserRolePojo actual) {
    return proxy(com.jooq.generate.tables.pojos.UserRolePojoAssert.class, com.jooq.generate.tables.pojos.UserRolePojo.class, actual);
  }

}
