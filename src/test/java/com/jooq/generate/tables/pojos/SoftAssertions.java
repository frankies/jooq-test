package com.jooq.generate.tables.pojos;

/**
 * Entry point for soft assertions of different data types.
 */
@javax.annotation.Generated(value="assertj-assertions-generator")
public class SoftAssertions extends org.assertj.core.api.SoftAssertions {

  /**
   * Creates a new "soft" instance of <code>{@link com.jooq.generate.tables.pojos.AuthorPojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.jooq.generate.tables.pojos.AuthorPojoAssert assertThat(com.jooq.generate.tables.pojos.AuthorPojo actual) {
    return proxy(com.jooq.generate.tables.pojos.AuthorPojoAssert.class, com.jooq.generate.tables.pojos.AuthorPojo.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link com.jooq.generate.tables.pojos.RolePojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.jooq.generate.tables.pojos.RolePojoAssert assertThat(com.jooq.generate.tables.pojos.RolePojo actual) {
    return proxy(com.jooq.generate.tables.pojos.RolePojoAssert.class, com.jooq.generate.tables.pojos.RolePojo.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link com.jooq.generate.tables.pojos.UserPojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.jooq.generate.tables.pojos.UserPojoAssert assertThat(com.jooq.generate.tables.pojos.UserPojo actual) {
    return proxy(com.jooq.generate.tables.pojos.UserPojoAssert.class, com.jooq.generate.tables.pojos.UserPojo.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link com.jooq.generate.tables.pojos.UserRoleCkPojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.jooq.generate.tables.pojos.UserRoleCkPojoAssert assertThat(com.jooq.generate.tables.pojos.UserRoleCkPojo actual) {
    return proxy(com.jooq.generate.tables.pojos.UserRoleCkPojoAssert.class, com.jooq.generate.tables.pojos.UserRoleCkPojo.class, actual);
  }

  /**
   * Creates a new "soft" instance of <code>{@link com.jooq.generate.tables.pojos.UserRolePojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created "soft" assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public com.jooq.generate.tables.pojos.UserRolePojoAssert assertThat(com.jooq.generate.tables.pojos.UserRolePojo actual) {
    return proxy(com.jooq.generate.tables.pojos.UserRolePojoAssert.class, com.jooq.generate.tables.pojos.UserRolePojo.class, actual);
  }

}
