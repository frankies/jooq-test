package com.jooq.generate.tables.pojos;

/**
 * {@link AuthorPojo} specific assertions - Generated by CustomAssertionGenerator.
 *
 * Although this class is not final to allow Soft assertions proxy, if you wish to extend it, 
 * extend {@link AbstractAuthorPojoAssert} instead.
 */
@javax.annotation.Generated(value="assertj-assertions-generator")
public class AuthorPojoAssert extends AbstractAuthorPojoAssert<AuthorPojoAssert, AuthorPojo> {

  /**
   * Creates a new <code>{@link AuthorPojoAssert}</code> to make assertions on actual AuthorPojo.
   * @param actual the AuthorPojo we want to make assertions on.
   */
  public AuthorPojoAssert(AuthorPojo actual) {
    super(actual, AuthorPojoAssert.class);
  }

  /**
   * An entry point for AuthorPojoAssert to follow AssertJ standard <code>assertThat()</code> statements.<br>
   * With a static import, one can write directly: <code>assertThat(myAuthorPojo)</code> and get specific assertion with code completion.
   * @param actual the AuthorPojo we want to make assertions on.
   * @return a new <code>{@link AuthorPojoAssert}</code>
   */
  @org.assertj.core.util.CheckReturnValue
  public static AuthorPojoAssert assertThat(AuthorPojo actual) {
    return new AuthorPojoAssert(actual);
  }
}
