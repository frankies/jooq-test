package com.jooq.generate.tables.pojos;

/**
 * Entry point for BDD assertions of different data types.
 */
@javax.annotation.Generated(value="assertj-assertions-generator")
public class BddAssertions {

  /**
   * Creates a new instance of <code>{@link com.jooq.generate.tables.pojos.AuthorPojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static com.jooq.generate.tables.pojos.AuthorPojoAssert then(com.jooq.generate.tables.pojos.AuthorPojo actual) {
    return new com.jooq.generate.tables.pojos.AuthorPojoAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link com.jooq.generate.tables.pojos.RolePojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static com.jooq.generate.tables.pojos.RolePojoAssert then(com.jooq.generate.tables.pojos.RolePojo actual) {
    return new com.jooq.generate.tables.pojos.RolePojoAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link com.jooq.generate.tables.pojos.UserPojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static com.jooq.generate.tables.pojos.UserPojoAssert then(com.jooq.generate.tables.pojos.UserPojo actual) {
    return new com.jooq.generate.tables.pojos.UserPojoAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link com.jooq.generate.tables.pojos.UserRoleCkPojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static com.jooq.generate.tables.pojos.UserRoleCkPojoAssert then(com.jooq.generate.tables.pojos.UserRoleCkPojo actual) {
    return new com.jooq.generate.tables.pojos.UserRoleCkPojoAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link com.jooq.generate.tables.pojos.UserRolePojoAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static com.jooq.generate.tables.pojos.UserRolePojoAssert then(com.jooq.generate.tables.pojos.UserRolePojo actual) {
    return new com.jooq.generate.tables.pojos.UserRolePojoAssert(actual);
  }

  /**
   * Creates a new <code>{@link BddAssertions}</code>.
   */
  protected BddAssertions() {
    // empty
  }
}
