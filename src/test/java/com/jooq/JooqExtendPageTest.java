package com.jooq;

import static com.jooq.generate.tables.TUser.USER;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.jooq.DSLContext;
import org.jooq.SelectLimitStep;
import org.jooq.impl.DSL;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.jooq.extend.PageResult;
import com.jooq.generate.tables.daos.UserDao;
import com.jooq.generate.tables.pojos.UserPojo;
import com.jooq.generate.tables.records.UserRecord;

/**
 *  修改生成器规则后，扩展功能测试
 *  
 * @author amoy
 * @version 1.0
 *
 */
public class JooqExtendPageTest extends BaseTest {
    

    @Autowired
    private UserDao userDao;
    
    @Test
    public void checkInject() {
        DSLContext dslContext = userDao.create();
        assertNotNull(dslContext);
    } 

    @Test
    public void fetchOneOptional() {
        Optional<UserPojo> u = userDao.fetchOneOptional(USER.ID.eq(1));
        softAssertions.assertThat(u).isPresent();
    }

    @Test
    public void fetch() {
        List<UserPojo> fetch = userDao.fetch(DSL.noCondition(), USER.USERID.desc());
        softAssertions.assertThat(fetch).size().isGreaterThan(0);
        softAssertions.assertThat(fetch).extracting(UserPojo::getId)
                                                       .contains(1);
    }

    @Test
    public void fetchPage() {
        //Insert 100;
        List<UserRecord> urs = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            UserRecord ur = dsl.newRecord(USER);
                 ur.setId( 100 + i);
               ur.setName("test-" + i);
               urs.add(ur);
        }
        dsl.batchInsert(urs).execute();
             
        
        //Query from page 1~3， into RECORD
        PageResult<UserPojo> pageQuery = new PageResult<>(1, 3);
        PageResult<UserPojo> pageResult = userDao.fetchPage(pageQuery, DSL.noCondition(), USER.ID.desc());
        this.softAssertions.assertThat(pageResult.getTotal())
                                    .as("分页总数").isEqualTo(3);
       this.softAssertions.assertThat(pageResult.getCurrentPage())                  
                                    .as("当前页码").isEqualTo(1);
       this.softAssertions.assertThat(pageResult.getPageSize())                  
                                   .as("每页数量").isEqualTo(3);
       
        List<UserPojo> pageData = pageResult.getData();
        this.softAssertions.assertThat(pageData)
                                    .as("分页数据存在").isNotNull().size().isPositive();
        
        this.softAssertions.assertThat(pageData).extracting(UserPojo::getId)
                                    .as("按降序排序").isSortedAccordingTo(Comparator.reverseOrder());   

        ///Paging query, into POJO
        SelectLimitStep<UserRecord> s = userDao.create()
                                                              .selectFrom(USER)
                                                              .where(USER.ID.greaterOrEqual(100))
                                                              .orderBy(USER.ID.desc()); //from User where id>100 order by id desc
        pageResult = userDao.fetchPage(pageQuery, s, UserPojo.class); //转成pojo
        this.softAssertions.assertThat(pageResult.getTotal())
                                        .as("分页总数").isEqualTo(3);
                                this.softAssertions.assertThat(pageResult.getCurrentPage())                  
                                        .as("当前页码").isEqualTo(1);
                                this.softAssertions.assertThat(pageResult.getPageSize())                  
                                       .as("每页数量").isEqualTo(3);
        
        pageData = pageResult.getData();
        this.softAssertions.assertThat(pageData)
                                  .as("分页数据存在").isNotNull().size().isPositive();
        
        this.softAssertions.assertThat(pageData).extracting(UserPojo::getId)
                                  .as("按降序排序").isSortedAccordingTo(Comparator.reverseOrder());         
        
        
        //Page query, start from 1st page, page size: 0.
        pageResult= userDao.fetchPage(new PageResult<>(1, 0), DSL.noCondition());                            
        pageData = pageResult.getData();
        this.softAssertions.assertThat(pageResult.getTotal())
                                    .as("分页总数").isEqualTo(0);
        this.softAssertions.assertThat(pageResult.getCurrentPage())                  
                                    .as("当前页码").isEqualTo(0);
        this.softAssertions.assertThat(pageResult.getPageSize())                  
                                    .as("每页数量").isEqualTo(0);
        
        pageData = pageResult.getData();
        this.softAssertions.assertThat(pageData)
              .as("分页数据不存在").isNotNull().size().isZero();
    }
}
