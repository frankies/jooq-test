package com.jooq;

import static com.jooq.generate.tables.TRole.ROLE;
import static com.jooq.generate.tables.TUser.USER;
import static com.jooq.generate.tables.TUserRole.USER_ROLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jooq.Result;
import org.junit.Test;

import com.jooq.generate.tables.pojos.UserPojo;
import com.jooq.generate.tables.records.UserRecord;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author amoy
 * @version 1.0
 *
 */
@Slf4j
public class JooqFetchTest extends BaseTest {

    @Test
    public void testFetch() {
        int c1 = this.countTable();
        Result<UserRecord> urs = dsl.selectFrom(USER).fetch();
        assertEquals(c1, urs.size());
    }

    @Test
    public void testFetchRecordMap() {
        UserPojo u = dsl.selectFrom(USER).where(USER.USERID.eq(1)).fetchOneInto(UserPojo.class);

        assertNotNull(u);
        assertNotNull(u.getUserid());
        assertEquals(Integer.valueOf(1), u.getUserid());
    }

    @Test
    public void testFetchJoin() {
        List<UserPojo> us = dsl.select().from(USER).join(USER_ROLE).on(USER_ROLE.USER_ID.eq(USER.USERID)).join(ROLE).on((ROLE.ID.eq(USER_ROLE.ROLE_ID)).and(ROLE.ROLE_NAME.eq("admin"))).fetch(r -> {
            UserPojo u = r.into(USER).into(UserPojo.class);
            return u;
        });

        assertTrue(us.size() > 0);
    }

    @Test
    public void testFetchField() {
        int c1 = this.countTable();
        List<String> ns = dsl.selectFrom(USER).fetch(USER.NAME);

        List<Integer> is = dsl.selectFrom(USER).fetch("ID", Integer.class);
        assertEquals(c1, ns.size());
        assertEquals(c1, is.size());
        for (String n : ns) {
            assertNotNull(n);
        }

        for (int i : is) {
            assertNotNull(i);
        }
    }

    @Test
    public void testFetchSetArray() {
        int c1 = this.countTable();
        UserRecord[] urs = dsl.selectFrom(USER).fetchArray();
        assertEquals(c1, urs.length);
        for (UserRecord r : urs) {
            assertNotNull(r);
        }

        Set<String> us = dsl.selectFrom(USER).where(USER.NAME.eq("Johnny Warlker")).fetchSet(USER.NAME);
        assertTrue(us.size() == 1);
    }
    
    @Test
    public void testFetchMap() {
        int c1 = this.countTable();
        Map<Integer, String> us = dsl.selectFrom(USER).fetchMap(USER.ID, USER.NAME);
        
        us.entrySet().stream()
           .forEach(e -> {
               log.info("{} : {}", e.getKey(), e.getValue());
           });
        assertTrue(us.size() == c1);
    }
    
    @Test
    public void testFetchGroup() { 
        Map<Integer, List<Integer>> g = dsl.selectFrom(USER_ROLE).fetchGroups(USER_ROLE.USER_ID, USER_ROLE.ROLE_ID);
        g.entrySet().stream()
           .forEach(e -> {
               log.info("{} : {}", e.getKey(), e.getValue());
           }); 
    }
}
