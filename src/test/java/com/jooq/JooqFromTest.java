package com.jooq;

import static com.jooq.generate.tables.TUser.USER;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jooq.impl.DSL;
import org.junit.Test;

import com.jooq.generate.tables.pojos.UserPojo;
import com.jooq.generate.tables.records.UserRecord;

/**
 *
 * @author amoy
 * @version 1.0
 *
 */
public class JooqFromTest extends BaseTest {

    @Test
    public void testFrom() {

        UserPojo u = new UserPojo();
        u.setId(1000);
        u.setName("test-1");

        UserRecord ur = dsl.newRecord(USER);
        ur.from(u);
        int c = ur.insert(USER.ID, USER.NAME);
        assertTrue(c == 1);
        Integer id = ur.getUserid();
        assertNotNull(id);
    }

    @Test
    public void testFromMap() {
        Map<String, Object> userDataMap = new HashMap<>();
        userDataMap.put("ID", 300);
        userDataMap.put("NAME", "ttt");

        UserRecord ur = dsl.newRecord(USER);
        ur.fromMap(userDataMap);
        int c = ur.insert(USER.ID, USER.NAME);
        assertTrue(c == 1);
        Integer id = ur.getUserid();
        assertNotNull(id);

    }

    @Test
    public void testFromArray() {
        int max = (Integer) dsl.select(DSL.max(USER.USERID)).from(USER).fetchOne().get(0);
        for (int i = 1; i <= 10; i++) {
            Object[] row = new Object[] { 100 + i, "test-1" + (100 + i) };
            UserRecord ur = dsl.newRecord(USER);
            ur.fromArray(row, USER.ID, USER.NAME );
            int c = ur.insert(USER.ID, USER.NAME);
            assertTrue(c == 1);
        }
        int max2 = (Integer) dsl.select(DSL.max(USER.USERID)).from(USER).fetchOne().get(0);
        assertNotNull(max2 - 10 == max);
    }
    
    @Test
    public void testFromArrayInBatch() {
        int max = (Integer) dsl.select(DSL.max(USER.USERID)).from(USER).fetchOne().get(0);
        List<UserRecord> us = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            Object[] row = new Object[] { 100 + i, "test-1" + (100 + i) };
            UserRecord ur = dsl.newRecord(USER);
            ur.fromArray(row, USER.ID, USER.NAME );
            us.add(ur);
        }
        dsl.batchInsert(us)
            .execute();
        int max2 = (Integer) dsl.select(DSL.max(USER.USERID)).from(USER).fetchOne().get(0);
        assertNotNull(max2 - us.size() == max);
    }
}
