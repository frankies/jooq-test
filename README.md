# 最佳实践：Jooq 查询工具的使用

[TOC]

[文档入口](https://frankies.gitlab.io/jooq-test/) [代码入口](https://gitlab.com/frankies/jooq-test)

## 简介
Jooq 是商业的Java ORM实现。2021-04-10 利用闲瑕时间，深入的体验了该工具的使用，包括源代码，采用以下技术：

- 代码引用库版本:
	- Spring framwork/Test `5.3.5`
	- jooq `3.14.8`
	- h2 `1.4.200`
	- 

- 代码构建工具：
	 - maven 
	   - markdown 转html
	   - jacoco 单元报告生成
	 - gitlab ci

## 配置(maven)

### 1) 添加Maven引用（jooq库和DB连接JDBC库，这里使用h2）

```xml

<dependency>
	<groupId>com.h2database</groupId>
	<artifactId>h2</artifactId>
	<version>1.4.200</version>
</dependency>
		
<dependency>
			<groupId>org.jooq</groupId>
			<artifactId>jooq</artifactId>
			<version>3.14.8</version>
</dependency>
<dependency>
	<groupId>org.jooq</groupId>
	<artifactId>jooq-meta</artifactId>
	<version>3.14.8</version>
</dependency>
<dependency>
	<groupId>org.jooq</groupId>
	<artifactId>jooq-codegen</artifactId>
	<version>3.14.8</version>
</dependency> 
```

### 2) 以文件存储模式启动h2，同时导入表创建脚本

表创建脚本见`src/test/resources/db.sql`

### 3) 利用Jooq代码生成器反向生成Java代码

- Maven 命令

```shell
mvn jooq-codegen:generate
```
- Java Jar命令

```shell
 java -jar jooq-codegen-3.14.8.jar org.jooq.codegen.GenerationTool jooq-gen.xml
```

生成规则配置见 `jooq-gen.xml`.

生成的源代码目录：`src/main/java/com/jooq/generate`

## 代码开发

### 1）CURD

包括： 增、删、查、改，还有**Batch处理**

- fetch* 方法查询

```java

//单条记录fetchOne/fetchAny/fetchSingle
Record u = dsl.selectFrom(USER)
           .limit(1)
           .fetchOne();
           
Record u = dsl.selectFrom(USER)
              .fetchAny();    
                  
//多条记录                                       
Result<UserRecord> u = dsl.fetch(USER);

//投影其中几列时，可利用 Record*类
Record2<String, Integer> record2 = dsl.select(USER.NAME, USER.ID).from(USER).fetchAny();

//只投影一列时
Record1<String>  nr =  dsl.select(USER.NAME).from(USER)
                          .where(USER.ID.eq(1))
                          .fetchOne();//要取得列的值，可以: nr.getValue(0)、nr.get(USER.NAME)
                          
String n =  dsl.selectFrom(USER)
		            .where(USER.ID.eq(1))
		            .fetchOne(USER.NAME);

```

- 新增insert 语句

```java
//表中的所有列，默认
UserRecord record = dsl.newRecord(USER);
record.setId(1003);
record.setName("test-1991");
record.insert();
//这时，可以取得自增主键的值
record.getUserid();


//可以限定Insert语句中的列
UserRecord ur =  dsl.newRecord(USER);
String val = "test-hello";
ur.setName(val);
ur.insert(USER.NAME);
   
```

```java
dsl.insertInto(USER, USER.ID, USER.NAME)
    .values(484, "name")
    .returning(USER.USERID)
    .fetchOne().getUserid(); //这时，可以取得自增主键的值
````

链式新增Insert

```java

//方式一，横向
 dsl.insertInto(USER, USER.ID, USER.NAME)
    .values(Long.valueOf(i).intValue(), "Johnny-" + i)
    .values(Long.valueOf(i + 1).intValue(), "sa-" + i)
    .values(Long.valueOf(i + 2).intValue(), "Joy-" + i)
    .execute();

//方法二，竖向
dsl.insertInto(USER)
   .set(USER.ID, Long.valueOf(i).intValue())
   .set(USER.NAME, "Johnny-" + i)
   .newRecord() //第一条insert记录
   .set(USER.ID, Long.valueOf(i + 1).intValue())
   .set(USER.NAME, "sa-" + i)
   .newRecord() //第二条insert记录
   .set(USER.ID, Long.valueOf(i + 2).intValue())
   .set(USER.NAME, "Joy-" + i) 
   .execute(); //第三条insert记录
```
```

插入时自动判断是否记录已存在

```java

//如果记录存在则忽略，也无SQL操作
 dsl.insertInto(USER, USER.USERID, USER.NAME)
     .values(1, "name")
     .onDuplicateKeyIgnore() 
     .execute();
    
//如果记录不存在，则更新某列的值
dsl.insertInto(USER, USER.USERID, USER.NAME)
   .values(1, "name")
   .onDuplicateKeyUpdate()
   .set(USER.NAME, actual)
   .execute();

```

进行批量Insert操作

```java
dsl.batch(
  dsl.insertInto(USER, USER.ID, USER.NAME).values(100, "dsl-100"),
  dsl.insertInto(USER, USER.ID, USER.NAME).values(101, "dsl-101"),
  dsl.insertInto(USER, USER.ID, USER.NAME).values(102, "dsl-102")
 ).execute();
     
//
dsl.batch(dsl.insertInto(USER, USER.ID, SER.NAME).values((Integer) null, null))
	 .bind(200 , "dsl-200"    )
	 .bind(201 , "dsl-201"    )
	 .bind(202 , "dsl-202"    )
   .execute();
```

- 更新insert 语句

```java

//近似SQL的写法
 dsl.update(USER)
     .set(USER.ID, 30)
     .set(USER.NAME, "test-39")
     .where(USER.USERID.eq(Integer.valueOf(1)))
     .execute();
     
//利用newRecord方法
UserRecord ur = dsl.newRecord(USER);
ur.setUserid(1);
ur.setName("test-39");
ur.update();

//先查出来，再更新，所有字段
UserRecord ur = dsl.selectFrom(USER)
                   .where(USER.ID.eq(1))
                   .fetchOne();
ur.setName("Test haha");
ur.setId(1000);
ur.update(); // 执行Update语句，更新两个字段

//先查出来，再更新，只有一个字段
UserRecord ur = dsl.selectFrom(USER)
                   .where(USER.ID.eq(1))
                   .fetchOne();
ur.setName("Test haha");
ur.setId(1000);
ur.update(USER.NAME); // 执行Update语句，只更新一个字段

//先查出来，再更新，通过.reset和..changed方法可以忽略某些列
UserRecord ur = dsl.selectFrom(USER)
                   .where(USER.ID.eq(1))
                   .fetchOne();
ur.setName("Test haha");
ur.setId(1000);
ur.changed(USER.NAME, false); //或 ur.reset(USER.NAME, false);
ur.update(); // 执行Update语句，不更新name 字段

```

批量更新

```java
UserRecord ur = new UserRecord();
ur.setUserid(1);
ur.setName("test-39");

UserRecord ur2 = new UserRecord();
ur2.setUserid(2);
ur2.setName("test-1300");

int[] cs = dsl.batchUpdate(Arrays.asList(ur, ur2))
              .execute();
```

- 删除delete

```java
//类SQL
int c = dsl.delete(USER)
           .where(USER.USERID.eq(Integer.valueOf(1)))
           .execute();
           
//利用record
UserRecord ur = dsl.newRecord(USER);
ur.setUserid(1); //这里如果不写，就等于整表中 user_id 为null的记录
int c = ur.delete();
        
```

批量删除

```java
 UserRecord ur1 = new UserRecord();
 ur1.setUserid(1);
 UserRecord ur2 = new UserRecord();
 ur2.setUserid(2);
 int[] c = dsl.batchDelete(Arrays.asList(ur1, ur2))
              .execute();
```

测试代码： `JooqCurdTest.java`

### 2) Fetch 开头的方法使用

- 普通 fetch


```java

Result<UserRecord> urs = dsl.selectFrom(USER).fetch();

//将结果转成 pojo对象
UserPojo u = dsl.selectFrom(USER).where(USER.USERID.eq(1)).fetchOneInto(UserPojo.class);

//也通过 Map函数把record into 成pojo 对象
List<UserPojo> us = dsl.select().from(USER)
                   .join(USER_ROLE).on(USER_ROLE.USER_ID.eq(USER.USERID))
                   .join(ROLE).on((ROLE.ID.eq(USER_ROLE.ROLE_ID))
                   .and(ROLE.ROLE_NAME.eq("admin"))
                    ).fetch(r -> {
            UserPojo u = r.into(USER).into(UserPojo.class);
            return u;
        });
```

- fetchArray
可以将结果转成数组

```java
 UserRecord[] urs = dsl.selectFrom(USER).fetchArray();
```
 
- fetchMap

可以转成按两字段作为key和value的`HashMap`

```java
 Map<Integer, String> us = dsl.selectFrom(USER).fetchMap(USER.ID, USER.NAME);
```

- fetchGroups

可以分组

```java
Map<Integer, List<Integer>> g = dsl.selectFrom(USER_ROLE).fetchGroups(USER_ROLE.USER_ID, USER_ROLE.ROLE_ID);
```

测试代码： `JooqFetchTest.java`

### 3) 将Pojo转Record类型的 `from` 方法使用

包括：
-  `from`

```java
UserPojo u = new UserPojo();
u.setId(1000);
u.setName("test-1");

UserRecord ur = dsl.newRecord(USER);
ur.from(u);
```

- `fromMap`

```java
Map<String, Object> userDataMap = new HashMap<>();
userDataMap.put("ID", 300);
userDataMap.put("NAME", "ttt");

UserRecord ur = dsl.newRecord(USER);
ur.fromMap(userDataMap);
//开始插入记录
int c = ur.insert(USER.ID, USER.NAME);

```
- `fromArray` 和 `batchInsert`批处理 

```java
//单条记录操作
Object[] row = new Object[] { 100 + i, "test-1" + (100 + i) };
UserRecord ur = dsl.newRecord(USER);
ur.fromArray(row, USER.ID, USER.NAME );
int c = ur.insert(USER.ID, USER.NAME);
```

```java
//批处理
List<UserRecord> us = new ArrayList<>();
for (int i = 1; i <= 100; i++) {
    Object[] row = new Object[] { 100 + i, "test-1" + (100 + i) };
    UserRecord ur = dsl.newRecord(USER);
    ur.fromArray(row, USER.ID, USER.NAME );
    us.add(ur);
}
dsl.batchInsert(us)
    .execute();
```


测试代码： `JooqFromTest.java`

### 4) 将Record转Pojo类型的 `into` 方法使用
包括：
- `into` 转成`Pojo`或 `Record*`类型

```java
 UserPojo u = dsl.fetchAny(USER).into(UserPojo.class);
      
 Record2<Integer, String> row = dsl.fetchAny(USER).into(USER.ID, USER.NAME);
      

```
- `intoArray` 转成 数组 `Object[]`

```java
        Object[]  row = dsl.fetchAny(USER).intoArray();
```
- `intoMap`转成`Map<String, Object>`

```java
Map<String, Object>  row = dsl.fetchAny(USER).intoMap();
```

-  `intoResultSet`转成`java.sql.ResultSet`

```java
        ResultSet rs = dsl.fetch(USER).intoResultSet();
        while (rs.next()) {
            assertNotNull(rs);
            assertNotNull(rs.getString(USER.NAME.getName()));
            assertNotNull(rs.getInt(USER.ID.getName()));
        }
```

测试代码： `JooqFromTest.java`

#### 5) 通用分页查询支持

```java
 PageResult<UserPojo> pageQuery = new PageResult<>(1, 3);
 PageResult<UserPojo> pageResult = userDao.fetchPage(pageQuery, DSL.noCondition(), USER.ID.desc());
 
```

| 方法 | 说明|
| ---- | ---- |  
| `getTotal` |  总记录数 |
| `getCurrentPage` | 当前页码，从1 开始算 |
| `getPageSize`  | 每页的记录数 |
| `getData`  |  查询记录列表 |

测试代码： `JooqExtendPageTest.java`

#### 6) Dao操作

```java

//根据主键查询单记录
UserPojo u = userDao.fetchOneByUserid(1);
u = userDao.findById(1);

//根据非主键查询列表
List<UserPojo> us = userDao.fetchByName("l");

//整表查询
us = userDao.findAll()

//利用 `assertj-assertions-generator-maven-plugin`插件得改成的UserPojoAssert类，进行hasUserId断言
UserPojoAssert.assertThat(u).hasUserid(1);

```
测试代码： `JooqExtendPageTest.java`


## 单元测试报告

- [Surefire 测试情况报告](surefire-report.html)
- [Jacoco 测试覆盖率报告](jacoco-ut/index.html)

关于Junit 断言工具assertj 请[参考](https://skyao.gitbooks.io/learning-java-unit-test/content/assertj/core_features/basic.html)

## 参考

- [英文官网 Jooq 使用手册](https://www.jooq.org/doc/3.14/manual/)
- [中文Jooq 教程](https://jooq.diamondfsd.com/learn/section-1-how-to-start.html)

{reviseDateTime=2021-04-10 19:19:19}
